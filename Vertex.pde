/*
 *
 */
import java.util.Set;
import java.util.HashSet;

class Vertex implements Shape {
  public int x;
  public int y;
  private final color fillColor;
  private color strokeColor;
  private final Set<Vertex> associations = new HashSet();
  private final int size; 

  public Vertex(int x, int y) {
    this.x = x;
    this.y = y;   
    this.fillColor = this.getColor();
    this.strokeColor = 0;
    this.size = 25;
  }   

  void paint() {    
    highlightOnMouseOver();    
    stroke(strokeColor);
    fill(fillColor);
    ellipse(x, y, size, size);
  }

  void highlightOnMouseOver() {
    if (isMouseOver(mouseX, mouseY)) {
      this.strokeColor = 255;
      // FIXME: should happen only once !!!
      // swingOnMouseOver();
    } else {
      this.strokeColor = 0;
    }
  }
  
  void swingOnMouseOver() {
    this.x += int(random(3))-1;
    this.y += int(random(3))-1;
  }

  void addAssociation(Vertex v) {
    associations.add(v);
  }

  color getColor() {
    int r = int(random(255));
    int g = int(random(255));
    int b = int(random(255));
    return color(r, g, b);
  }  

  boolean isMouseOver(int mouseX, int mouseY) {
    int padding = size / 2;
    int leftEdge = x - padding;
    int rightEdge = x + padding;
    int topEdge = y - padding;
    int bottomEdge = y + padding;

    //println("LeftEdge: " + leftEdge);
    //println("RightEdge: " + rightEdge);
    //println("TopEdge: " + topEdge);
    //println("BottomEdge: " + bottomEdge);
    //println("MouseX: " + mouseX);
    //println("MouseY: " + mouseY);

    if (leftEdge <= mouseX && mouseX <= rightEdge) {
      if (topEdge <= mouseY && mouseY <= bottomEdge) {        
        return true;
      }
    }

    return false;
  }

  @Override
    public boolean equals(Object o) {
    if (o instanceof Vertex) {
      if (this.identifier == ((Vertex)o).identifier) 
        return true;
    }
    return false;
  }
}