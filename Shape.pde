/*
 *
 */

abstract interface Shape {
  long identifier = System.currentTimeMillis();
  abstract void paint();
  abstract boolean isMouseOver(int mouseX, int mouseY);
}