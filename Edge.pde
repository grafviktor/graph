/*
 *
 */

class Edge implements Shape {

  private Vertex v1;
  private Vertex v2;

  public Edge(Vertex v1, Vertex v2) {
    this.v1 = v1;
    this.v2 = v2;
  }

  boolean isMouseOver(int mouseX, int mouseY) {
    return false;
  }

  void paint() {    
    stroke(0);
    pushMatrix();
    translate(0, 0, -1);
    line(v1.x, v1.y, v2.x, v2.y);
    popMatrix();
  }
}