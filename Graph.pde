/*
 *
 */

import java.util.List;
import java.util.ArrayList;

List<Shape> shapes;
//private int counter = 0; 
private boolean draggingStarted = false;
private Vertex sourceVertex = null;

public Graph() {
  shapes = new ArrayList();
  ClassLoader.getSystemClassLoader().setDefaultAssertionStatus(true);
}

void setup() {
  size(600, 600, P3D);
  smooth(8);
  strokeWeight(2);
}

void draw() {
  background(125);
  drawShapes();
}

void mouseClicked() {
  Vertex v = new Vertex(mouseX, mouseY);
  shapes.add(v);
}

void mouseDragged() {  
  if (!draggingStarted && (sourceVertex = getCurrentVertex()) != null) {
    draggingStarted = true;
  }
}

void mouseReleased() {
  if (draggingStarted) {
    Vertex targetVertex = null;
    if ((targetVertex = getCurrentVertex()) != null) {      
      createVertexAssociation(sourceVertex, targetVertex);
      createNewEdge(sourceVertex, targetVertex);
    }
    draggingStarted = false;
  }
}

void createVertexAssociation(Vertex source, Vertex target) {
  source.addAssociation(target);
  target.addAssociation(source);
}

void createNewEdge(Vertex source, Vertex target) {
  Edge e = new Edge(source, target);
  shapes.add(e);
}

void drawShapes() {
  for (Shape shape : shapes) {
    shape.paint();
  }
}

Vertex getCurrentVertex() {
  for (Shape shape : shapes) {
    if (shape instanceof Vertex) { 
      if (shape.isMouseOver(mouseX, mouseY)) {
        return (Vertex)shape;
      }
    }
  }
  // FIXME: return Shape.EMPTY;
  return null;
}